import React from "react";
import Navbar from "./navbar/Navbar";
import "./Style.css";

const Mycv = () => {
    return(
        <div style={{ backgroundColor: "#f2f2f2"}}>

            <Navbar />


         
            <div className="ui container"> 
                <div className="ui segment">
                <br/><br/><br/><br/>

                    {/* page1 */}
                    <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <div className="ui small image">
                                <img src="zaid.png" />
                            </div>
                        </div>
                        <div className="twelve wide column">
                        <h1>ZAID KHAN</h1>
                            <h3>Full-stack Web Developer</h3>
                            <h4>Over 14 years of experience in front-end and back-end web-development. Ambitious and communicative. Good mentor and tech lead.</h4> 
                            <h4>Currently available. Willing to relocate to: US, UK, AU, NZ, CA, Latin America, Spain or Kraków; or can work remotely.</h4>
                        </div>
                    </div>

                    <br/><br/><br/>

                    {/* Page2 */}
                    <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">APPUNITE</button>
                            <h4>Oct 2018 - Aug 2019</h4>
                            <a>appunite.com</a>
                        </div>
                        <div className="twelve wide column">
                            <h3>Experience</h3>
                            <h4>Full-stack Web Developer</h4>
                            <p>Led development of a new front-end for one of the biggest American companies in the hospitality and entertainment industry.</p>
                            <ul>
                                <li>Bootstrapped, developed and led most of the decisions throughout the project. Configured the CI/CD pipeline.</li>
                                <li>Helped recruit, onboard and mentor new team members, both in Poland and the US.</li>
                                <li>Convinced the upper management to let the team use best available tools (e.g. TypeScript, React with Hooks, automatic accessibility/visual/unit tests).</li>
                                <li>Actively helped manage, estimate and scope out the project.</li>
                            </ul>
                        </div>
                    </div>


                    <br/><br/>

                    {/* Page3 */}
                    <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">RECRUITEE</button>
                            <h4>Sep 2017 - Jun 2018</h4>
                            <a>recruitee.com</a>
                        </div>
                        <div className="twelve wide column">
                            <h4>Full-stack Web & Mobile Developer</h4>
                            <p>Led development of a new front-end for one of the biggest American companies in the hospitality and entertainment industry.</p>
                            <ul>
                                <li>Bootstrapped, developed and led most of the decisions throughout the project. Configured the CI/CD pipeline.</li>
                                <li>Helped recruit, onboard and mentor new team members, both in Poland and the US.</li>
                                <li>Convinced the upper management to let the team use best available tools (e.g. TypeScript, React with Hooks, automatic accessibility/visual/unit tests).</li>
                                <li>Actively helped manage, estimate and scope out the project.</li>
                            </ul>
                        </div>
                    </div>


                    <br/><br/>

                    {/* Page4 */}
                    <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">TRIPSOMNIA</button>
                            <h4>Jan 2016 - Aug 2017</h4>
                            <a>tripsomnia.com</a>
                        </div>
                        <div className="twelve wide column">
                            <h4>Lead Full-stack Web Developer</h4>
                            <p>Built an entire SaaS and marketplace platform, that allows to book and manage tourist tours and attractions all over the world.</p>
                            <ul>
                                <li>Bootstrapped, developed and led most of the decisions throughout the project. Configured the CI/CD pipeline.</li>
                                <li>Helped recruit, onboard and mentor new team members, both in Poland and the US.</li>
                                <li>Convinced the upper management to let the team use best available tools (e.g. TypeScript, React with Hooks, automatic accessibility/visual/unit tests).</li>
                                <li>Actively helped manage, estimate and scope out the project.</li>
                            </ul>
                        </div>
                    </div>

                <br/><br/>

                 {/* Page5 */}
                 <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">FESTBLAST</button>
                            <h4>Jan 2014 - Dec 2015</h4>
                            <a>festblast.com</a>
                        </div>
                        <div className="twelve wide column">
                            <h4>Lead Full-stack Web & Mobile Developer</h4>
                            <p>Built and maintained a social platform, that lets users easily find the best music fests, organize their trip and personalize their festival experience.</p>
                            <ul>
                                <li>Bootstrapped, developed and led most of the decisions throughout the project. Configured the CI/CD pipeline.</li>
                                <li>Helped recruit, onboard and mentor new team members, both in Poland and the US.</li>
                                <li>Convinced the upper management to let the team use best available tools (e.g. TypeScript, React with Hooks, automatic accessibility/visual/unit tests).</li>
                                <li>Actively helped manage, estimate and scope out the project.</li>
                            </ul>
                        </div>
                    </div>

                <br/><br/>

                 {/* Page6 */}
                 <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">FREELANCE</button>
                            <h4>Aug 2005 - Aug 2017</h4>
                            <a>jtom.me</a>
                        </div>
                        <div className="twelve wide column">
                            <h4>Full-stack Web Developer</h4>
                            <ul>
                                <li>Created multiple web applications, participating in the whole process of their development: product design and estimation, code design and development, DevOps, UI/UX design, product launch and maintenance. (jtom.me/portfolio)</li>
                                <li>Bootstrapped, developed and led most of the decisions throughout the project. Configured the CI/CD pipeline.</li>
                                <li>Helped recruit, onboard and mentor new team members, both in Poland and the US.</li>
                                <li>Convinced the upper management to let the team use best available tools (e.g. TypeScript, React with Hooks, automatic accessibility/visual/unit tests).</li>
                                <li>Actively helped manage, estimate and scope out the project.</li>
                            </ul>

                     
                            <h3>Skills</h3>
                            <button className="ui white button">ELIXIR</button> 
                            <button className="ui white button">TYPESCRIPT</button>
                            <button className="ui white button">JAVASCRIPT</button>  
                            <button className="ui white button">RUBY</button>
                            <button className="ui white button">REACT</button>
                            <button className="ui white button">ANGULAR</button>
                            <button className="ui white button">REDUX</button> <br/><br/>
                            <button className="ui white button">ELASTICSEARCH</button>
                            <button className="ui white button">REDIS</button>
                            <button className="ui white button">BUSINESS ORIENTED DEVELOPMENT</button>
                            <button className="ui white button">UX DESIGN</button>
                        </div>
                    </div>

                <br/><br/><br/>

                 {/* Page7 */}
                 <div className="doubling stackable ui two column grid">
                        <div className="four wide column center aligned">
                            <button className="ui white basic button">POZNAN UNIVERSITY OF TECHNOLOGY</button>
                            <h4>2012 - 2016</h4>
                            <a>put.poznan.pl</a>
                        </div>
                        <div className="twelve wide column">
                            <h3>Education</h3>
                            <h4>BSc in Computer Science</h4>
                            <ul>
                                <li>Served as a president of AKAI, a students' research circle focused on organizing workshops and lectures about web development.</li>
                                <li>Co-created the new main public website of Poznań University of Technology.</li>
                            </ul>
                            <br/>
                            <h3>Other perks</h3>
                            <ul>
                                <li>Great public speaker. (Long-time member of an english-speaking Toastmasters club.)</li>
                                <li>Knowledge of algorithms and data structures. (Scored a 3rd laurate place in IV Polish Informatics Olympiad of middle school students.)</li>
                                <li>Speaks native Polish, fluent English, conversational Spanish and basic Russian.</li>
                                <li>In his free time, likes to blog, dance, play tennis, and travel.</li>
                            </ul>
                        </div>
                    </div>

            </div>
        </div>

            <br/><br/><br/><br/>   

        </div>
    );
}

export default Mycv;