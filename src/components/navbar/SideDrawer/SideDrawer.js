import React from 'react';

import './SideDrawer.css';

const sideDrawer = props => {
  let drawerClasses = 'side-drawer';
  if (props.show) {
    drawerClasses = 'side-drawer open';
  }
  return (
    <nav className={drawerClasses}>




      <div class="ui container ">
          <div class="doubling stackable ui menu">
            
              {/* <div className="left item">
              <h4>PROFIT<sub>4</sub>MONEY</h4> 
              </div> */}
          
              <div class="item">
                <a href="/portfolio"> PORTFOLIO </a>
              </div>
              <div class="item">
                <a href="/myoffer"> MY OFFER </a>
              </div>
              <div class="item">
                <a href="/contactme"> CONTACT ME </a>
              </div>
              <div class="item">
                <a href="/mycv"> MY CV </a>
              </div>
              <div class="item">
                <a href="/techtalks"> MY TECH TALKS </a>
              </div>
              <div class="item">
                <a href="/myblog"> PERSONAL BLOG </a>
              </div>

              <div class="item i:hover">
                <a href="#"><i className="black big mail icon"></i></a>
                <a href="#"><i className="black big linkedin icon"></i></a>
                <a href="#"><i className="black big github icon"></i></a>
                <a href="https://www.facebook.com/"><i className="black big facebook icon"></i></a>
                <a href="#"><i className="black big twitter icon"></i></a>
              </div>

  
          </div>
        </div>


      {/* <ul>
        <li>
          <a href="/">Products</a>
        </li>
        <li>
          <a href="/">Users</a>
        </li>
      </ul> */}
    </nav>
  );
};

export default sideDrawer;
