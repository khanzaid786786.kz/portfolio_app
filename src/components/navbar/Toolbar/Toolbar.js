import React from 'react';

import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';
import './Toolbar.css';




const toolbar = props => (
  <header className="toolbar">
    <nav className="toolbar__navigation">
        <div className="toolbar__toggle-button">
            <DrawerToggleButton click={props.drawerClickHandler} />
        </div>
        <div className="toolbar__logo">
      
          <div className="ui mini circular image">
            <img src="zaid.png" />
          </div> 

          <a href="/"> ZAID KHAN</a>

        </div>
        <div className="spacer" />
        <div className="toolbar_navigation-items ">
       
        <div class="ui secondary menu">
            <div class="item">
              <a  href="/portfolio"> PORTFOLIO </a>
            </div>
            <div class="item">
              <a href="/myoffer"> MY OFFER </a>
            </div>
            <div class="item">
              <a href="/contactme"> CONTACT ME </a>
            </div>
            <div class="item">
              <a href="/mycv"> MY CV </a>
            </div>
            <div class="item">
              <a href="/techtalks"> MY TECH TALKS </a>
            </div>
            <div class="item">
              <a href="/myblog"> PERSONAL BLOG </a>
            </div>
            <div class="item i:hover">
                <a href="#"><i className="black big mail icon"></i></a>
                <a href="#"><i className="black big linkedin icon"></i></a>
                <a href="#"><i className="black big github icon"></i></a>
                <a href="https://www.facebook.com/"><i className="black big facebook icon"></i></a>
                <a href="#"><i className="black big twitter icon"></i></a>
            </div>
        </div>
 

        </div>
    </nav>
  </header>
);

export default toolbar;
