import React from "react";
import "./Style.css";
import Navbar from "./navbar/Navbar";

const Card = () => {
    return(
        <div className="cardcon">


            <Navbar />
            <div className="ui fluid container">
                {/* <div className="ui small circular  image">
                    <img className="img1" src="cv.jpg" />
                </div> */}
                <h1 className="cardhead">Hi, I'm Zaid</h1>
                <p className="cardpara">I m a freelance web developer specialized in a front-end and back-end and a web development.</p>
                <p className="cardpara1">Get in touch</p>
                <i className="yellow big mail icon i1"></i>
                <i className="green big linkedin icon i2"></i>
                <i className="yellow big github icon i3"></i>
                <i className="green big facebook icon i4"></i>
                <i className="yellow big twitter icon i5"></i>
                <button className="ui big green basic button cardbtn1">PORTFOLIO</button> 
                <button className="ui big yellow basic button cardbtn2">MY OFFER</button> 


                {/* footer */}
                <div className="ui container center aligned">
                    <div className="ui secondary menu">
                        <div className="left item">
                            <h5>ZaidKhanSince@1995copywrite.com</h5>
                        </div>
                        <div className="item">
                            About
                        </div>
                        <div className="item">
                            Contact
                        </div>
                        <div className="item">
                            Help
                        </div>
                    </div>
                    
                </div>
                

            </div>


        </div>
    );
}

export default Card;