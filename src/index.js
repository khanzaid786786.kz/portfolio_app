import React from  "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route} from 'react-router-dom';


import Card from "./components/Card";
import Portfolio from "./components/Portfolio";
import Offer from "./components/Offer";
import Contact from "./components/Contact";
import Mycv from "./components/Mycv";
import Techtalk from "./components/Techtalk";
import Blog from "./components/Blog";


ReactDOM.render(

    <BrowserRouter>

     
        <Route exact path="/" component={Card}/>
        <Route exact path="/portfolio" component={Portfolio}/>
        <Route exact path="/myoffer" component={Offer}/>
        <Route exact path="/contactme" component={Contact}/>
        <Route exact path="/mycv" component={Mycv}/>
        <Route exact path="/techtalks" component={Techtalk}/>
        <Route exact path="/myblog" component={Blog}/>


    </BrowserRouter>,
    document.getElementById("root"));